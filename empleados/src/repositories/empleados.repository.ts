import {DefaultCrudRepository} from '@loopback/repository';
import {Empleados, EmpleadosRelations} from '../models';
import {EmpleadosDataSource} from '../datasources';
import {inject} from '@loopback/core';

export class EmpleadosRepository extends DefaultCrudRepository<
  Empleados,
  typeof Empleados.prototype.Id,
  EmpleadosRelations
> {
  constructor(
    @inject('datasources.empleados') dataSource: EmpleadosDataSource,
  ) {
    super(Empleados, dataSource);
  }
}
